# Datasets of semantic similarity between human's protein-coding genes

This repository contains two datasets:

1. **Genes**
  - GID (the Entrez identifier of the gene);
  - Name (the gene name);
  - Type (the gene type);
  - BagOfCUIs (the list of concepts associated to the gene);
  - Summary (the gene summary).

2. **Relationships** between the genes for which the relationship's weight is >= 0.1
  - Weight (the measure of the relatedness between the linked gene pair);
  - SharedCUIs (the list of concepts shared by the linked gene pair).


## "Neo4j friendly"

You can create a Neo4j database importing these datasets using the `neo4j-import` tool.

###### Install Neo4j (on a Linux machine)

Get it from `https://neo4j.com/download/`.

Extract in a directory of your choice (e.g. `/opt/`).

###### Populate the database

    # /opt/neo4j-community-{YOUR_VERSION}/bin/neo4j-import --into {DESTINATION} --nodes "{PATH_OF_GENES_DATASET}" --relationships "{PATH_OF_SIMILARITIES_DATASET}" --delimiter "|" --array-delimiter ";"

Launch Neo4j:

    # /opt/neo4j-community-{YOUR_VERSION}/bin/neo4j start


## QgeneS
These datasets are used in [QgeneS](https://www.github.com/mattux/qgenes).
